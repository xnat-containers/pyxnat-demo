import os
from os import listdir
from os.path import isfile, join
from os import walk
import pyxnat
from pyxnat import Interface

# See https://pyxnat.github.io/pyxnat/features/index.html for pyxnat syntax

host = os.environ.get('XNAT_HOST')
user = os.environ.get('XNAT_USER')
password = os.environ.get('XNAT_PASS')
projectLabel = os.environ.get('PROJECT')

# Connect to XNAT server
xnat_connection = Interface(server=host, user=user, password=password)

# Print project info
if xnat_connection.select.project(projectLabel).exists():
    project = xnat_connection.select.project(projectLabel)
    print(project)
else:
    print('Project: ' + projectLabel + ' not found')

# Assume project files live at /input, just list files
projectDir = '/input/'
f = []
for (dirpath, dirnames, filenames) in walk(projectDir):
    f.extend(filenames)

print(f)
